import sys
import logging
from termcolor import colored
from inventorymanager import InventoryManager
from billmanager import BillManager

if len(sys.argv) == 3 and sys.argv[1] == 'inventory':
    inventory_manager = InventoryManager()
    if sys.argv[2] == 'add':
        print('Enter new product details')
        product = dict()
        product['id'] = input(colored('product ID', 'yellow'))
        product['name'] = input(colored('product Name', 'green'))
        product['price'] = input(colored('product Price', 'red'))
        product['stock'] = input(colored('product Stock', 'white'))
        inventory_manager.add(product)
        inventory_manager.commit()

    elif sys.argv[2] == 'list':
        inventory_manager.print_all()
    
    elif sys.argv[2] == 'search':
        inventory_manager.print_all(inventory_manager.search(input('Try entering Id or Name to search:\n')))

    elif sys.argv[2] == 'modify':
        search_results = inventory_manager.search(input('Try entering Id or Name to search:\n'))
        inventory_manager.print_all(search_results)
        index = int(input('Which one?'))
        index = index - 1
        if index > len(search_results):
            print(colored('Are you kidding?'))
            exit(0)
        product = search_results[index]
        print('Selected Product', end=' ')
        inventory_manager.print_one(product)
        entered = input('product Name' + colored('('+product['name']+')?', 'green'))
        if entered != '':
            product['name'] = entered
        entered = input('product Price' + colored('('+product['price']+')?', 'green'))
        if entered != '':
            product['price'] = entered
        entered = input('product Stock' + colored('('+product['stock']+')?', 'green'))
        if entered != '':
            product['stock'] = entered
        inventory_manager.modify(product)
        inventory_manager.commit()

    elif sys.argv[2] == 'delete':
        search_results = inventory_manager.search(input('Try entering Id or Name to search:\n'))
        inventory_manager.print_all(search_results)
        index = int(input('Which one?'))
        index = index - 1
        if index > len(search_results):
            print(colored('Are you kidding?'))
            exit(0)
        product = search_results[index]
        inventory_manager.delete(product['id'])
        inventory_manager.commit()

else:
    bill_manager = BillManager()
    inventory_manager = InventoryManager()
    while(1):
        entered = input('Try entering Id or Name to search:<end to finish>\n')
        if entered == 'end':
            break
        search_results = inventory_manager.search(entered)
        inventory_manager.print_all(search_results)
        index = int(input('Which one to add?'))
        index = index - 1
        if index > len(search_results):
            print(colored('Are you kidding?'))
            exit(0)
        product = search_results[index]
        quantity = int(input('How many?'))
        bill_manager.add_item(product, quantity)
    bill_manager.generate_bill()
    entered = input('Is this ok?(y/n)')
    if entered == 'y':
        bill_manager.ok()
        print('Thank you!')