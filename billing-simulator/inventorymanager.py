import os
import logging
from termcolor import colored
import csv
from collections import OrderedDict

PRODUCTSFILE = './products.csv'

class InventoryManager():

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        if not os.path.exists(PRODUCTSFILE):
            self.logger.error(colored('Product Database not Found', 'red'))
            exit(0)
        csv_reader = csv.DictReader(open(PRODUCTSFILE, 'r'))
        self.products = dict()
        for row in csv_reader:
            self.products[row['id']] = row
        self.logger.info(colored('Loaded Database into memory', 'green'))

    def add(self, new_product):
        if new_product['id'] in self.products.keys():
            self.logger.warning(colored('Add: Not Added! Product should have unique ID', 'yellow'))
            return
        self.products[new_product['id']] = new_product

    def search(self, search_key):
        search_results = list()
        for product in self.products.values():
            if search_key in product['id'] or search_key in product['name']:
                search_results.append(product)
        return search_results

    def delete(self, p_id):
        if p_id in self.products.keys():
            del self.products[p_id]
        else:
            self.logger.warning(colored('Delete: Product not found!', 'yellow'))

    def modify(self, product):
        self.products[product['id']] = product

    def reduce_item_stock(self, p_id, quanity):
        self.products[p_id][stock] = self.products[p_id][stock] - quanity

    def print_all(self, products = None):
        if products == None:
            products = self.products.values()
        print(colored('id', 'yellow'), colored('name', 'green'), colored('price', 'red'), colored('stock', 'white'), sep='\t')
        for product in products:
            self.print_one(product)

    def print_one(self, product):
        print(colored(product['id'], 'yellow'), colored(product['name'], 'green'), colored(product['price'], 'red'), colored(product['stock'], 'white'), sep='\t')
    
    def commit(self):
        ordered_fieldnames = OrderedDict([('id',None),('name',None),('price',None),('stock',None)])
        csv_writer = csv.DictWriter(open(PRODUCTSFILE, 'w'), ordered_fieldnames)
        csv_writer.writeheader()
        for product in self.products.values():
            csv_writer.writerow(product)
