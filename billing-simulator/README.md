# supermarket-billing-simulator-python
CEGLUG python task

## How to Use

### Billing
`python3 billing.py` 
Generate a bill for checkout.

### Add Product to Inventory
`python3 billing.py inventory add` 
Add new Product to Database.

### List Products in Inventory
`python3 billing.py inventory list` 
List all the products in Database.

### Search Products in Inventory
`python3 billing.py inventory search` 
Search for a product.

### Modify Product
`python3 billing.py inventory modify` 
Modify already existing product.

### Delete Product from Inventory
`python3 billing.py inventory delete` 
Delete from Database.