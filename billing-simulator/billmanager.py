import os
import logging
from termcolor import colored
from inventorymanager import InventoryManager

class BillManager():

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.bill = list()
        self.inventorymanager = InventoryManager()

    def add_item(self, product, quanity):
        product['stock'] = str(int(product['stock']) - quanity)
        self.inventorymanager.modify(product)
        self.bill.append([product['id'], product['name'], product['price'], quanity])

    def generate_bill(self):
        total = 0
        for item in self.bill:
            total = total + float(item[2])*float(item[3])
            print(colored(item[0], 'yellow'), colored(item[1], 'green'), colored(item[2], 'red'), colored(item[3], 'white'), sep='\t')
        print('Total Amount:', colored(total, 'red'))

    def ok(self):
        self.inventorymanager.commit()