import csv
from collections import OrderedDict

data = list()

numbers = next(csv.reader(open('number.csv')))
fruits = next(csv.reader(open('fruits.csv')))
prices = next(csv.reader(open('price.csv')))
rottens = next(csv.reader(open('rotten.csv')))

for index in range(100):
    if numbers[index] == '':
        if index%2==0:
            continue
        else:
            numbers[index] = str(index)
    data.append({ 'number': numbers[index], 'fruit': fruits[index], 'price': prices[index], 'rotten': rottens[index] })

for index, item in enumerate(data):
    if item['price'] == '':
            item['price'] = '0.0'
    if item['rotten'] == '1' or item['rotten'] == 't':
        item['price'] = '0.0'
        item['rotten'] = 't'
    else:
        item['price'] = str(float(item['price']))
        item['rotten'] = 'f'
    if item['fruit'] == '':
        item['fruit'] = data[index - 10]['fruit']

ordered_fieldnames = OrderedDict([('number',None),('fruit',None),('price',None),('rotten',None)])
csv_writer = csv.DictWriter(open('data.csv', 'w'), ordered_fieldnames)
csv_writer.writeheader()
csv_writer.writerows(data)
